import QtQuick 2.7
import QtQuick.Controls 2.0

Label {
    id: root
    text: ""
    horizontalAlignment: Text.AlignHCenter
    width: parent.width

    x: (contentWidth > parent.width) ? root.contentWidth : 0

    NumberAnimation on x {
        running: ((root.contentWidth > root.width) ? true : false)
        from: root.contentWidth
        to: root.contentWidth * (-1)
        duration: root.contentWidth * 20
        loops: Animation.Infinite
        easing.type: Easing.Linear;
    }
}
