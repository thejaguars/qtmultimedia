import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

DelegateModel {

    Item {
        id: root_item
        width: root.width
        height: root.height * 0.125

        property var cur: 0

        property int min: (duration/60);
        property int sec: (duration%60);
        property string dur: min + ":" + ("0" + sec).slice(-2)

        property string artist_duration: ((artist == "") ? "Artista Desconhecido" : artist) + " (" + dur + ")"
        property bool isHighlightedItem: ListView.isCurrentItem

        GridLayout {
            id: list_item
            rows: 3
            columns: 2
            rowSpacing: 0
            columnSpacing: parent.height * 0.15
            anchors.leftMargin: parent.height * 0.15
            anchors.rightMargin: parent.height * 0.15
            anchors.fill: parent

            Item {
                id: list_title
                Layout.row: 1
                Layout.column: 1
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true

                Label {
                    text: (title == "") ? "Título Desconhecido" : title;
                    font.family: agfa_rotis_bold.name
                    font.pixelSize: parent.height * 0.8
                    font.bold: true
                    color: "#444"
                }
            }

            Item {
                id: list_buttons_wrapper
                Layout.row: 1
                Layout.column: 2
                Layout.rowSpan: 2
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumWidth: parent.width * 0.3

                RowLayout {
                    id: list_buttons
                    anchors.fill: parent
                    spacing: 0
                    visible: isHighlightedItem ? true : false

                    Button {
                        id: list_play_btn
                        Layout.preferredWidth: parent.width * 0.2
                        Layout.preferredHeight: parent.height
                        Layout.alignment: Qt.AlignRight

                        //property string explanation: "Busca por nome de música."
                        property string icon_source: "./icons/play_icon.svg"

                        KeyNavigation.left: allsongs_btn
                        KeyNavigation.right: list_addnext_btn

                        style: ButtonStyle {
                            background: Rectangle {
                                width: parent.width * 0.8
                                height: parent.width * 0.8
                                radius: parent.height * 0.08
                                border.color : (control.hovered | control.activeFocus) ? "#aaa" : "transparent"
                                border.width : (control.hovered | control.activeFocus) ? 1 : 0
                                color: (control.hovered | control.activeFocus) ? "white" : "transparent"
                                anchors.centerIn: parent

                                Image {
                                    source: list_play_btn.icon_source
                                    width: parent.width * 0.6
                                    height: parent.width * 0.6
                                    anchors.centerIn: parent
                                }
                            }
                        }

                        Keys.onEnterPressed: {
                            musicsModel[0].reproduzir(path);
                            timer.start();
                            time_end.text= dur;
                            time_end.val = duration;

                            title_song.text = (title == "") ? "Título Desconhecido" : title;
                            artist_song.text = (artist == "") ? "Artista Desconhecido" : artist;
                            album_song.text = (album == "") ? "Álbum Desconhecido" : album;

                            playing = true;
                        }

                        Keys.onSpacePressed:{

                            if(playing){
                                musicsModel[0].pausar();
                                timer.stop();
                                playing=false;

                            }else{
                                musicsModel[0].resume();
                                timer.restart();
                                playing=true;}
                        }
                    }

                    Button {
                        id: list_addnext_btn
                        Layout.preferredWidth: parent.width * 0.2
                        Layout.preferredHeight: parent.height
                        Layout.alignment: Qt.AlignRight

                        //property string explanation: "Busca por nome de música."
                        property string icon_source: "./icons/next_icon.svg"

                        KeyNavigation.left: list_play_btn
                        KeyNavigation.right: list_add_btn

                        style: ButtonStyle {
                            background: Rectangle {
                                width: parent.width * 0.8
                                height: parent.width * 0.8
                                radius: parent.height * 0.08
                                border.color : (control.hovered | control.activeFocus) ? "#aaa" : "transparent"
                                border.width : (control.hovered | control.activeFocus) ? 1 : 0
                                color: (control.hovered | control.activeFocus) ? "white" : "transparent"
                                anchors.centerIn: parent

                                Image {
                                    source: list_addnext_btn.icon_source
                                    width: parent.width * 0.6
                                    height: parent.width * 0.6
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Button {
                        id: list_add_btn
                        Layout.preferredWidth: parent.width * 0.2
                        Layout.preferredHeight: parent.height
                        Layout.alignment: Qt.AlignRight

                        //property string explanation: "Busca por nome de música."
                        property string icon_source: "./icons/add_icon.svg"

                        KeyNavigation.left: list_addnext_btn
                        KeyNavigation.right: list_edit_btn

                        style: ButtonStyle {
                            background: Rectangle {
                                width: parent.width * 0.8
                                height: parent.width * 0.8
                                radius: parent.height * 0.08
                                border.color : (control.hovered | control.activeFocus) ? "#aaa" : "transparent"
                                border.width : (control.hovered | control.activeFocus) ? 1 : 0
                                color: (control.hovered | control.activeFocus) ? "white" : "transparent"
                                anchors.centerIn: parent

                                Image {
                                    source: list_add_btn.icon_source
                                    width: parent.width * 0.6
                                    height: parent.width * 0.6
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Button {
                        id: list_edit_btn
                        Layout.preferredWidth: parent.width * 0.2
                        Layout.preferredHeight: parent.height
                        Layout.alignment: Qt.AlignRight

                        //property string explanation: "Busca por nome de música."
                        property string icon_source: "./icons/edit_icon.svg"

                        KeyNavigation.left: list_add_btn
                        KeyNavigation.right: list_delete_btn

                        style: ButtonStyle {
                            background: Rectangle {
                                width: parent.width * 0.8
                                height: parent.width * 0.8
                                radius: parent.height * 0.08
                                border.color : (control.hovered | control.activeFocus) ? "#aaa" : "transparent"
                                border.width : (control.hovered | control.activeFocus) ? 1 : 0
                                color: (control.hovered | control.activeFocus) ? "white" : "transparent"
                                anchors.centerIn: parent

                                Image {
                                    source: list_edit_btn.icon_source
                                    width: parent.width * 0.6
                                    height: parent.width * 0.6
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Button {
                        id: list_delete_btn
                        Layout.preferredWidth: parent.width * 0.2
                        Layout.preferredHeight: parent.height
                        Layout.alignment: Qt.AlignRight

                        //property string explanation: "Busca por nome de música."
                        property string icon_source: "./icons/delete_icon.svg"

                        KeyNavigation.left: list_edit_btn

                        style: ButtonStyle {
                            background: Rectangle {
                                width: parent.width * 0.8
                                height: parent.width * 0.8
                                radius: parent.height * 0.08
                                border.color : (control.hovered | control.activeFocus) ? "#aaa" : "transparent"
                                border.width : (control.hovered | control.activeFocus) ? 1 : 0
                                color: (control.hovered | control.activeFocus) ? "white" : "transparent"
                                anchors.centerIn: parent

                                Image {
                                    source: list_delete_btn.icon_source
                                    width: parent.width * 0.6
                                    height: parent.width * 0.6
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    states: [
                            State { when: isHighlightedItem;
                                PropertyChanges {   target: list_buttons; opacity: 1.0    }
                            },
                            State { when: !isHighlightedItem;
                                PropertyChanges {   target: list_buttons; opacity: 0.0    }
                            }
                        ]

                    transitions: Transition {
                        NumberAnimation { property: "opacity"; duration: 1000}
                    }
                }
            }

            Item {
                id: list_artist_duration
                Layout.row: 2
                Layout.column: 1
                Layout.fillWidth: true
                Layout.fillHeight: true

                Label {
                    text: artist_duration
                    font.family: agfa_rotis.name
                    font.pixelSize: parent.height * 0.6
                    verticalAlignment: Text.AlignTop
                    color: "#444"
                }
            }

            Rectangle {
                id: list_underline
                Layout.row: 3
                Layout.column: 1
                Layout.columnSpan: 2
                Layout.preferredWidth: parent.width * 0.985
                Layout.preferredHeight: 1
                color: "#888"
            }

        }

        onFocusChanged: {
            if(root_item.focus){
                list_play_btn.focus = true
            }
        }

        Timer {
            id: timer
            interval: 1000
            running: true
            repeat: true
            property int tempoinsecs;
            property int tmin:0;
            property int tsec: 0;
            property string t:"0:00";

            onTriggered: {

                progress_bar.value = 0;
                if(playing){
                    tempoinsecs = musicsModel[0].progress()/1000;
                    tmin =(tempoinsecs/60);
                    tsec =(tempoinsecs%60);
                    t = tmin + ":" + ("0" + tsec).slice(-2);
                    time_counter.text = t;
                    cur = (tempoinsecs/time_end.val);
                    progress_bar.value = cur;
                }else
                    progress_bar.value = cur;
            }
        }
    }
}
