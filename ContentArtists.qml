import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

ListView {
    id: root
    anchors.fill: parent
    currentIndex: focus ? 0 : -1

    property var byArtist: Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', root)
    property var songsFromArtist: Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', root)

    highlight: Component {
        id: root_highlight

        Rectangle {
            width: root.width
            height: root.height * 0.125
            border.color: "#888"
            border.width: 1
            radius: root.height * 0.008
            color: "#ddd"
        }
    }

    model: lisdaDeArtista

    ListArtists {
        id: lisdaDeArtista

        Component.onCompleted: {
            var artistName = ""
            var countSongs = 0
            var indexByArtist = 0

            for(var i = 0; i < sortedByArtist.count; i++) {
                if(countSongs == 0) {
                    artistName = sortedByArtist.get(i).artist
                    countSongs = 1;
                    byArtist.append({artist: artistName , numTitle: countSongs});
                }
                else if(artistName == sortedByArtist.get(i).artist) {
                    countSongs++;
                    byArtist.setProperty(indexByArtist, "numTitle", countSongs);
                }
                else {
                    artistName = sortedByArtist.get(i).artist
                    countSongs = 1;
                    byArtist.append({artist: artistName , numTitle: countSongs});
                    indexByArtist++;
                }
            }

            model = byArtist;
        }
    }

    ListTitles {
        id: listaDeTitulos
    }

    function selectTitleFromArtist(selectedArtist) {
        for(var i = 0; i < sortedByArtist.count; i++) {
            if(selectedArtist == sortedByArtist.get(i).artist) {
                songsFromArtist.append(sortedByArtist.get(i));
            }
        }

        listaDeTitulos.model = songsFromArtist;
        root.model = listaDeTitulos;
    }
}
