import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0


//    Image {
//        id: logo
//        Layout.alignment: Qt.AlignCenter
//        source: "./icons/logo.svg"
//        height: parent.height*0.3
//        fillMode: Image.PreserveAspectFit
//        horizontalAlignment: Image.AlignRight
//    }


RowLayout {
    id: menu_wrapper
    anchors.fill: parent

    signal handlerLoader(string page)

    Button {
        id: images_btn
        Layout.fillWidth: true
        Layout.preferredHeight: parent.height*0.4

        KeyNavigation.right: musics_btn

        focus: true

        style: ButtonStyle { background: Rectangle { color: "transparent"; border.color: "transparent" } }

        Image {
            id: images_img
            width: parent.width*0.7
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "icons/images_main.svg"

        }

        ColorOverlay {
            anchors.fill: images_img
            source: images_img
            color: {
                if (images_btn.hovered) {"gray"}
                else if (images_btn.activeFocus) {"gray"}
                else {"transparent"}
            }
        }
        Keys.onEnterPressed: {
            focus = true
            handlerLoader("pageImage.qml")
        }
        onClicked: { handlerLoader("pageImage.qml") }

    }

    Button {
        id: musics_btn
        Layout.fillWidth: true
        Layout.preferredHeight: images_btn.height
        KeyNavigation.right: videos_btn

        style: ButtonStyle { background: Rectangle { color: "transparent"; border.color: "transparent" } }

        Image {
            id: musics_img
            width: parent.width*0.7
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "icons/musics_main.svg"
        }

        ColorOverlay {
            anchors.fill: musics_img
            source: musics_img
            color: {
                if (musics_btn.hovered) {"gray"}
                else if (musics_btn.activeFocus) {"gray"}
                else {"transparent"}
            }
        }
        Keys.onEnterPressed: {
            focus = true
            handlerLoader("pageMusic.qml")
        }
        onClicked: { handlerLoader("pageMusic.qml") }
    }

    Button {
        id: videos_btn
        Layout.fillWidth: true
        Layout.preferredHeight: images_btn.height

        KeyNavigation.right: config_btn

        style: ButtonStyle { background: Rectangle { color: "transparent"; border.color: "transparent" } }

        Image {
            id: videos_img
            width: parent.width*0.7
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "icons/videos_main.svg"
        }

        ColorOverlay {
            anchors.fill: videos_img
            source: videos_img
            color: {
                if (videos_btn.hovered) {"gray"}
                else if (videos_btn.activeFocus) {"gray"}
                else {"transparent"}
            }
        }

        Keys.onEnterPressed: {
            focus = true
            handlerLoader("pageVideo.qml")
        }
        onClicked: { handlerLoader("pageVideo.qml") }
    }

    Button {
        id: config_btn
        Layout.fillWidth: true
        Layout.preferredHeight: images_btn.height

        style: ButtonStyle { background: Rectangle { color: "transparent"; border.color: "transparent" } }

        Image {
            id: config_img
            width: parent.width*0.7
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "icons/config_main.svg"
        }

        ColorOverlay {
            anchors.fill: config_img
            source: config_img
            color: {
                if (config_btn.hovered) {"gray"}
                else if (config_btn.activeFocus) {"gray"}
                else {"transparent"}
            }
        }

        Keys.onEnterPressed: {
            focus = true
            handlerLoader("pageConfig.qml")
        }
        onClicked: { handlerLoader("pageConfig.qml") }
    }
}

