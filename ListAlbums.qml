import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2

DelegateModel {

    delegate: Item {
        id: root_item
        width: root.width
        height: root.height

        property bool isHighlightedItem: GridView.isCurrentItem

        Button {
            id: album_icon
            width: root.cellWidth
            height: root.cellHeight

            KeyNavigation.left: albuns_btn

            Rectangle {
                anchors.fill: parent
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
            }

            Rectangle {
                id: bbbb
                width: parent.width
                height: album_name.height
                anchors.bottom: parent.bottom
                color: "black"
                opacity: 0.3
            }

            Label {
                id: album_name
                width: parent.width
                height: contentHeight * 1.2
                anchors.centerIn: bbbb

                text: album
                font.pixelSize: album_icon.width * 0.13
                font.family: agfa_rotis_bold.name
                font.bold: true

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.Wrap
                color: "white"
            }
            Keys.onEnterPressed: selectTitleFromAlbum(album);

        }

        onFocusChanged: {
            if(root_item.focus){
                album_icon.focus = true
            }
        }
    }
}

