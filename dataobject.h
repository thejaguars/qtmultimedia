#ifndef DATAOBJECT_H
#define DATAOBJECT_H
#endif // DATAOBJECT_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QList>
#include <QQuickView>
#include <QQmlContext>
#include <QMediaPlayer>
#include <QDir>
#include <QTimer>
#include <QImage>
//#include <mpegproperties.h>
//#include <mpegfile.h>
//#include <sstream>
//#include <string>
//#include <fileref.h>
//#include <tag.h>

//#include<id3v2tag.h>
//#include<id3v2frame.h>
//#include<id3v2header.h>
//#include <attachedpictureframe.h>

class DataObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString artist READ artist WRITE setArtist NOTIFY artistChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString album READ album WRITE setAlbum NOTIFY albumChanged)
    //Q_PROPERTY(int duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(QImage coverArt READ coverArt WRITE setCoverArt NOTIFY coverArtChanged)

    public:
        ~DataObject(){}
        explicit DataObject(QObject *parent = 0,QDir* mDir=new QDir("D:\\"),QString mFileShortName=""): QObject(parent), m_player(new QMediaPlayer(this)){


         //DataObject(QDir* mDir, QString mFileShortName){

            QString mediaCompleteName = mDir->absolutePath()+"/"+mFileShortName;
            m_path = mediaCompleteName;

            qInfo() << "Media complete name " << mediaCompleteName;
            QString mediaFileName = mFileShortName.left(mFileShortName.length()-4);
//            TagLib::FileRef file(QFile::encodeName(mediaCompleteName).constData());
//            TagLib::MPEG::File *mpegf = dynamic_cast<TagLib::MPEG::File *>(file.file());

//            TagLib::String tagTitle = file.tag()->title();
//            m_title= QString::fromStdWString(tagTitle.toWString());

//            TagLib::String tagArtist = file.tag()->artist();
//            m_artist= QString::fromStdWString(tagArtist.toWString());

//            TagLib::String tagAlbum = file.tag()->album();
//            m_album = QString::fromStdWString(tagAlbum.toWString());

//            TagLib::MPEG::Properties properties(mpegf);
//            m_duration = properties.length();

//            TagLib::ID3v2::Tag *id3v2tag = mpegf->ID3v2Tag();
//            TagLib::ID3v2::FrameList frame;
//            TagLib::ID3v2::AttachedPictureFrame *coverImg = NULL;
//            if(id3v2tag) {
//                frame = id3v2tag->frameListMap()["APIC"];
//                if(!frame.isEmpty()) {
//                    coverImg = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(frame.front());
//                    m_coverArt.loadFromData((const uchar *) coverImg->picture().data(), coverImg->picture().size());
//                    qInfo() << "Existe imagem ";
//                }
//                else {
//                    qInfo() << "Não existe imagem.";
//                }
//            }

             //TagLib::String tagDuration= calcDuration(properties.lengthInSeconds());
        }

        QString path()const{
            return m_path;
        }

        QString title() const{
            return m_title;
        }

        QString artist() const{
            return m_artist;
        }

        QString album() const{
            return m_album;
        }

        QImage coverArt() const {
            return m_coverArt;
        }

        int duration()const{
            return m_duration;
        }

        void setPath(QString path){
            m_path = path;
            emit pathChanged(path);
        }

        void setTitle(QString title){
            m_title = title;
            emit titleChanged(title);
        }

        void setArtist(QString artist){
            m_artist = artist;
            emit artistChanged(artist);
        }

        void setAlbum(QString album){
            m_album = album;
            emit albumChanged(album);
        }

        void setCoverArt(QImage coverArt) {
            m_coverArt = coverArt;
            emit coverArtChanged(coverArt);
        }

        void setDuration(int duration){
            m_duration = duration;
            emit durationChanged(duration);
        }


        Q_INVOKABLE void reproduzir(QString path) const {
            m_player->stop(); //Stop any other media playing before
            m_player->setMedia(QUrl::fromLocalFile(path));
            m_player->setVolume(80);//set default volume at 80%
            m_player->play();
            emit m_player->seekableChanged(true);
        }

        Q_INVOKABLE void pausar() {
               qInfo()<<"pause";
               m_player->pause();
        }

        Q_INVOKABLE void resume(){
            qInfo()<<"resume";
            m_player->play();
        }

        Q_INVOKABLE int progress(){
            return m_player->position() ;
        }

    signals:
        void pathChanged(QString);
        void titleChanged(QString);
        void artistChanged(QString);
        void albumChanged(QString);
        void coverArtChanged(QImage);
        void durationChanged(int);

    private:
        QString m_path;
        QString m_title;
        QString m_artist;
        QString m_album;
        QImage m_coverArt;
        int m_duration;
        QMediaPlayer *m_player;
//        TagLib::String calcDuration(int lengthInSeconds){
//            //int hours = (lengthInSeconds/3600);
//            int min = (lengthInSeconds/60);
//            int sec = (lengthInSeconds%60);
//            std::stringstream ss;
//            ss << min<<":"<< sec ;
//            std::string s = ss.str();
//            TagLib::String str(s);
//            return str;
//        }
};
