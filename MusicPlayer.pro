QT += qml quick core gui multimedia

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

win32 {
TAGLIBDIR = $$quote(C:/Libraries/taglib-1.11)
INCLUDEPATH += $TAGLIBDIR
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ape)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/asf)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/flac)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/it)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mod)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mp4)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpc)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v1)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v2)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v2/frames)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/flac)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/opus)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/speex)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/vorbis)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff/aiff)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff/wav)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/s3m)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/toolkit)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/trueaudio)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/wavpack)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/xm)
LIBS += $(SUBLIBS) -L$$quote($${TAGLIBDIR}/taglib-release) -ltaglib
}

macx{
TAGLIBDIR = $$quote(/Users/Paolla/Libraries/taglib-1.11.1)
INCLUDEPATH += $TAGLIBDIR
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ape)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/asf)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/flac)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/it)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mod)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mp4)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpc)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v1)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v2)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/mpeg/id3v2/frames)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/flac)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/opus)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/speex)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/ogg/vorbis)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff/aiff)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/riff/wav)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/s3m)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/toolkit)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/trueaudio)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/wavpack)
INCLUDEPATH += $$quote($${TAGLIBDIR}/taglib/xm)
LIBS += $(SUBLIBS) -L$$quote($${TAGLIBDIR}/taglib-release) -ltaglib
}

unix{
QMAKE_CXXFLAGS += -I /usr/include/taglib -I /usr/include/taglib-extras
#QMAKE_LIBS += -ltag
LIBS += $(SUBLIBS)  -L/usr/lib -lpthread #-ltag
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    dataobject.h

DISTFILES +=
