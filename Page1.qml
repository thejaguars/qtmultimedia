import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ListView{
    id: listView
    anchors.fill:parent

    highlight: Component {
        Rectangle {
            width: 200
            height: 50
            color: "steelblue"
        }
    }
    model: musicsModel
    delegate:Rectangle{
        color:"pink"
        border.width: 5;
        border.color: "black"
        height: 50
        Text{
            text: title
            font.pointSize: 10
            padding: 10
        }
        MouseArea{
            hoverEnabled: true;
            onClicked: console.log("Clicked")
        }
    }

}
