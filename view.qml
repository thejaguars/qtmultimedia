import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: applicationWindow
    visible: true
    minimumHeight: 480
    minimumWidth: 640
    title: qsTr("Music Player")

    property bool playing: false;

    FontLoader {
        id: agfa
        source: "fonts/agfarotissansserif.ttf"
    }
    FontLoader {
        id: agfabold
        source: "fonts/agfarotissansserif-bold.ttf"
    }

    // ---------------------------------------------
    //  MAIN WINDOW
    // ---------------------------------------------
    Rectangle {
        id: window_main
        anchors.fill: parent

        gradient: Gradient {
            GradientStop { position: 0.5; color: "white" }
            GradientStop { position: 1.0; color: "#ccc" }
        }

        // -----------
        // BOX HEADER
        // -----------

        Rectangle {
            id: box_header
            color: "transparent"
            anchors.top: parent.top
            anchors.topMargin: parent.width*0.05
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.04
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.85
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.32

            Text {
                id: item_title
                width: 211
                text: qsTr("Músicas")
                anchors.top: parent.top
                anchors.topMargin: parent.height*0.00
                anchors.left: parent.left
                anchors.leftMargin: parent.height*0.00
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height*0.00
                font.family: "agfaboldf"
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: parent.height*1.2
            }
        }

        // ----------
        // BOX LOGO
        // ----------

        Rectangle {
            id: box_logo
            color: "transparent"
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.05
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.05
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.85
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.7

            Image{
                id: img_logo
                anchors.fill: parent
                source: "pix/multimedia-f.jpg"
            }
        }

        // ------------
        // BOX CENTRAL
        // ------------

        Rectangle {
            id: box_central
            color: "transparent"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.15
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.2
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.05
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.05

            // ------------------------------------------
            //     COLUMN MENU ICONS
            // ------------------------------------------

            ColumnLayout {
                id: col_menuIcons
                Layout.minimumWidth: parent.width * 0.05
                Layout.minimumHeight: parent.height
                height: parent.height
                width: parent.width*0.05

                ToolButton {
                    id: search_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: parent.top

                    property string explanation: "Busca por nome de música."
                    Image{
                        id: img_search
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/musica_searcher.svg"
                    }
                    onFocusChanged: {
                        if(search_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: music_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: search_btn.bottom

                    property string explanation: "Exibe todas as músicas."
                    Image{
                        id: img_music_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/music_player_sign.svg"
                    }
                    onFocusChanged: {
                        if(music_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: artist_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: music_btn.bottom

                    property string explanation: "Exibe músicas por artista."
                    Image{
                        id: img_artist_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/music_social_group.svg"
                    }
                    onFocusChanged: {
                        if(artist_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: album_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: artist_btn.bottom

                    property string explanation: "Exibe músicas por álbum."
                    Image{
                        id: img_album_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/cd_player.svg"
                    }
                    onFocusChanged: {
                        if(album_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: playlists_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: album_btn.bottom

                    property string explanation: "Exibe playlists."
                    Image{
                        id: img_playlists_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/options_list.svg"
                    }
                    onFocusChanged: {
                        if(playlists_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: lyrics_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.top: playlists_btn.bottom

                    property string explanation: "Exibe letra de música atual."
                    Image{
                        id: img_lyrics_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/playlist.svg"
                    }
                    onFocusChanged: {
                        if(lyrics_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }

                ToolButton {
                    id: settings_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.bottom: home_btn.top

                    property string explanation: "Configurações de Música."

                    Image{
                        id: img_settings_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/settings_work_tool.svg"
                    }
                    onFocusChanged: {
                        if(settings_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
                ToolButton {
                    id: home_btn
                    Layout.minimumWidth: parent.width
                    Layout.minimumHeight: Layout.minimumWidth
                    anchors.bottom: parent.bottom

                    property string explanation: "Retorna a tela inicial."
                    Image{
                        id: img_home_btn
                        width: parent.width*0.6
                        height:width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "pix/home_icon_silhouette.svg"

                    }
                    onFocusChanged: {
                        if(home_btn.focus){
                            txt_func.text = explanation
                        }
                    }
                }
            }

            // ------------------------------
            //     COLUMN TRACK LIST
            // ------------------------------

            ListView{
                id: col_tracklist
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.08
                anchors.right: parent.right
                anchors.rightMargin: parent.width*0.32
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height*0.00
                anchors.top: parent.top
                anchors.topMargin: parent.height*0.00
                anchors.fill:parent

                //            ScrollBar.vertical: ScrollBar {
                //                id: scrollBar
                //                active: vbar.active               //BARRA DE ROLAGEM. FAZER FUNCIONAR DIREITO
                //                size: parent.height

                //            }
                //            Keys.onUpPressed: scrollBar.decrease()
                //            Keys.onDownPressed: scrollBar.increase()

                focus:true
                highlight: Component {
                    Rectangle {
                        id: component1
                        width: parent.width
                        height: parent.height*0.3
                        color: "#d6d6d6"
                        border.color: "gray"
                        border.width: 1
                        radius: 10
                    }
                }

                model: musicsModel

                delegate:Rectangle{
                    id: component2
                    width:parent.width
                    height: 60
                    color: "transparent"
                    property int min: (duration/60);
                                    property int sec: (duration%60);
                                    property string dur: sec<10?(min+":0"+ sec):min+":"+ sec
                    Text{
                        text: title
                        font.bold: true
                        font.pointSize: parent.height*0.25
                        padding: 10
                        font.family: "agfa"
                    }
                    Text{
                        text: dur
                        font.italic: true
                        font.pointSize: parent.height*0.20
                        padding:5
                        leftPadding: 10
                        anchors.bottom: parent.bottom
                        font.family: "agfa"

                    }
                    Keys.onEnterPressed: {
                        reproduzir(path);
                        tempo_countdown.text= dur;
                        tempo_countdown.val = duration;
                        timer.start();
                        playing=true;
                    }

                    onFocusChanged: {
                        if(focus){
                            nome_musica.text= title
                            nome_artista.text= artist
                            nome_album.text=album

                            if(!playing){
                             tempo_countdown.text= dur;
                             tempo_countdown.val = duration;
                            }
                        }
                    }
                    Timer {
                        id: timer
                        interval: 1000
                        running: true;
                        repeat: true
                        property int tempoinsecs;
                        property int tmin:0;
                        property int tsec: 0;
                        property string t:"0:00";

                        onTriggered: {

                            progressbar.value = 0;
                            if(playing){
                                tempoinsecs = progress()/1000;
                                tmin =(tempoinsecs/60);
                                tsec =(tempoinsecs%60);
                                t= tsec<10?(tmin+":0"+ tsec):tmin+":"+ tsec;
                                tempo_decorrido.text = t;
                                progressbar.value = (tempoinsecs/tempo_countdown.val);
                            }
                        }
                    }
                }
            }

            // ------------------------------------
            //     COLUMN TRACK INFO
            // ------------------------------------

            Rectangle {
                id: col_track_info
                color: "transparent"
                anchors.top: parent.top
                anchors.topMargin: parent.height*0.00
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height*0.00
                anchors.right: parent.right
                anchors.rightMargin: parent.width*0.00
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.70

                // ----------------
                // ITEM ALBUM IMAGE
                // ----------------
                Image {
                    id: item_artwork
                    width: height
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: parent.height*0
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: parent.height*0.50
                    source: "pix/21.jpg"                    // VALOR A SER MUDADO
                }

                // ---------------------
                // ITEM TRACK DETAILS
                // ---------------------
                Column {
                    id: item_details
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width*0.05
                    anchors.left: parent.left
                    anchors.leftMargin: anchors.rightMargin
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: parent.height*0.28
                    anchors.top: parent.top
                    anchors.topMargin: parent.height*0.52


                    Text{
                        id: nome_musica
                        text: "track_name"
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: parent.height*0.2
                        font.family: "agfa"
                        font.weight: Font.DemiBold
                    }
                    Text{
                        id: nome_artista
                        text: "artist"
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: nome_musica.font.pointSize
                        font.family: "agfa"
                    }
                    Text{
                        id: nome_album
                        text: "album"
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: nome_musica.font.pointSize
                        font.family: "agfa"
                    }
                    Text{
                        id: nome_playlist
                        text: "playlist"
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: nome_musica.font.pointSize
                        font.family: "agfa"
                    }
                }

                // -----------------
                // ITEM MUSIC PLAYER
                // -----------------
                Rectangle {
                    id: item_player
                    color: "transparent"
                    anchors.top: parent.top
                    anchors.topMargin: parent.height*0.75
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width*0.00
                    anchors.left: parent.left
                    anchors.leftMargin: anchors.rightMargin
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0


                    // -----------------
                    // Botões do player
                    // -----------------
                    Row {
                        id: player_btns
                        anchors.left: parent.left
                        anchors.leftMargin: 0
                        anchors.topMargin: 0
                        anchors.right: parent.right
                        anchors.rightMargin: 0
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: parent.height*0.4
                        spacing: parent.width*0.05
                        anchors.top: parent.top


                        ToolButton {
                            id: repetir
                            width: parent.width*0.15
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            Image{
                                width: parent.width
                                height:width
                                anchors.fill: parent
                                source: "pix/clockwise_refresh_arrow.svg"

                            }
                        }

                        ToolButton {
                            id: voltar
                            width: parent.width*0.15
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            Image{
                                width: parent.width
                                height:width
                                id: image_voltar
                                anchors.fill: parent
                                source: "pix/saltar_a_pista_anterior.svg"
                            }
                        }


                        ToolButton {
                            id: play
                            width: parent.width*0.20
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            Image{
                                width: parent.width
                                height:width
                                id: image_play
                                anchors.fill: parent
                                source: "pix/music_player_play.svg"
                            }
                        }

                        ToolButton {
                            id: passar
                            width: parent.width*0.15
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            Image{
                                width: parent.width
                                height:width
                                id: image_passar
                                anchors.fill: parent
                                source: "pix/skip_track_option.svg"
                            }
                        }

                        ToolButton {
                            id: shuffle
                            width: parent.width*0.15
                            height: width
                            anchors.verticalCenter: parent.verticalCenter
                            Image{
                                width: parent.width
                                height:width
                                id: image_shuffle
                                anchors.fill: parent
                                source: "pix/suffle_option.svg"
                            }
                        }

                    }
                    // -------------------
                    // Barra de Reprodução
                    // -------------------
                    Rectangle {
                        id: reproducao_box
                        color: "#00000000"
                        anchors.left: parent.left
                        anchors.leftMargin: 0
                        anchors.right: parent.right
                        anchors.rightMargin: 0
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: parent.height*0.00
                        anchors.top: parent.top

                        anchors.topMargin: parent.height*0.70

                        ProgressBar {
                            id:progressbar
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            anchors.top: parent.top
                            anchors.topMargin: 0
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: parent.height*0.8
                            maximumValue: 1
                            minimumValue: 0
                            value: 0
                            style: ProgressBarStyle {
                                background: Rectangle {
                                    color: "lightgray"
                                    border.color: "gray"
                                    border.width: 1
                                }
                                progress: Rectangle {
                                    color: "gray"
                                    border.color: "darkgray"
                                }
                            }
                        }

                        Text{
                            id: tempo_decorrido
                            color: "#232333"
                            text: "0:00"            // VALOR A SER MUDADO
                            anchors.right: parent.right
                            anchors.rightMargin: parent.width*0.75
                            anchors.left: parent.left
                            anchors.leftMargin: parent.width*0.00
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: parent.height*0.1
                            anchors.top: parent.top
                            anchors.topMargin: parent.height*0.35
                            font.pointSize: parent.height*0.5
                            font.family: "agfa"
                            property int val: 0
                        }
                        Text{
                            id: tempo_countdown
                            color: "#232333"
                            text: "10:00"           // VALOR A SER MUDADO
                            anchors.right: parent.right
                            anchors.rightMargin: parent.width*0.00
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: parent.height*0.1
                            anchors.top: parent.top
                            anchors.topMargin: parent.height*0.35
                            font.pointSize: parent.height*0.5
                            font.family: "agfa"
                            property int val: 0
                        }

                    }
                }
            }
        }

        // --------------
        // BOX MENU INFO
        // --------------

        Rectangle {
            id: box_func
            radius: 3
            border.color: "gray"
            color: "#E3E3E3" //whitesmoke
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.87
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.05
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.05
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.width*0.04
            RowLayout {
                id: row_func
                anchors.fill: parent
                Image {
                    id: img_func
              //      Layout.leftMargin:
                    Layout.maximumHeight: parent.height*0.5
                    Layout.maximumWidth: Layout.maximumHeight
                    Layout.alignment: Qt.AlignHCenter || Qt.AlignVCenter
                    source: "pix/music_player_sign.svg"
                }

                Text {
                    id: txt_func
                    text: "Texto explicativo para a funcionalidade selecionada"
                    Layout.alignment: Qt.AlignVCenter
                    font.family: "agfa"
                    font.pointSize: parent.height*0.4
                    color: "gray"

                }
            }
        }
    }
}
