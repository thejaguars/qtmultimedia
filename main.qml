import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2

ApplicationWindow {
    id: mainWindow
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: qsTr("MediaCenter")

    //ATIVA TELA CHEIA AUTOMÁTICA
    visibility: Window.FullScreen

    property var sortedByTitle
    property var sortedByArtist
    property var sortedByAlbum

    FontLoader {
        id: agfa_rotis
        source: "./fonts/agfarotissansserif.ttf"
    }

    FontLoader {
        id: agfa_rotis_bold
        source: "./fonts/agfarotissansserif-bold.ttf"
    }

    Component.onCompleted: {
        sortedByTitle = Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', mainWindow);
        sortedByArtist = Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', mainWindow);
        sortedByAlbum = Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', mainWindow);

        for(var i = 0; i < musicsModel.length; i++) {
            sortedByTitle.append(musicsModel[i]);
            sortedByArtist.append(musicsModel[i]);
            sortedByAlbum.append(musicsModel[i]);
        }

        for(var i = 0; i < sortedByTitle.count; i++) {
            for(var j = 0; j < i; j++) {
                if(sortedByTitle.get(i).title < sortedByTitle.get(j).title){
                    sortedByTitle.move(i,j,1);
                }
            }
        }

        for(var i = 0; i < sortedByArtist.count; i++) {
            for(var j = 0; j < i; j++) {
                if(sortedByArtist.get(i).artist < sortedByArtist.get(j).artist) {
                    sortedByArtist.move(i, j, 1);
                }
                else if(sortedByArtist.get(i).artist == sortedByArtist.get(j).artist) {
                    if(sortedByArtist.get(i).title < sortedByArtist.get(j).title){
                        sortedByTitle.move(i, j, 1);
                    }
                }
            }
        }

        for(var i = 0; i < sortedByAlbum.count; i++) {
            for(var j = 0; j < i; j++) {
                if(sortedByAlbum.get(i).album < sortedByAlbum.get(j).album){
                    sortedByAlbum.move(i,j,1);
                }
                else if(sortedByAlbum.get(i).album == sortedByAlbum.get(j).album){
                    if(sortedByAlbum.get(i).title < sortedByAlbum.get(j).title){
                        sortedByTitle.move(i, j, 1);
                    }
                }
            }
        }
    }

    Rectangle {
        id: background
        anchors.fill: parent
        Keys.onEscapePressed: {
                    Qt.quit();
        }
        gradient: Gradient {
            GradientStop { position: 0.5; color: "white" }
            GradientStop { position: 1.0; color: "#ccc" }
        }

        signal handlerLoader(string page)

        // ------------
        // PAGES WRAPPER
        // ------------
        Loader{
            id: pageLoader
            anchors.fill: parent;
            anchors.margins: parent.height * 0.05
            source: "pageMainMenu.qml"
            focus: true
        }

        Connections {
            target: pageLoader.item
            onHandlerLoader: {
                pageLoader.source = page;
                pageLoader.focus = true;
            }
        }
    }
}
