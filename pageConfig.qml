import QtQuick 2.0

Rectangle {
    id: root
    anchors.fill: parent
    color: "yellow"

    signal handlerLoader(string page)

    Text {
        text: "Clique para voltar"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: handlerLoader("pageMainMenu.qml")
    }
}
