import QtQuick 2.7
import QtQml.Models 2.2
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

GridView {
    id: root
    anchors.fill: parent
    currentIndex: focus ? 0 : -1
    cellWidth: root.width * ((width < 700) ? 0.245 : 0.195)
    cellHeight: root.cellWidth

    property var byAlbum: Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', root)
    property var songsFromAlbum: Qt.createQmlObject('import QtQuick 2.2; \ListModel {}', root)

    KeyNavigation.left: albuns_btn

    highlight: Component {
        id: root_highlight

        Rectangle {
            anchors.fill: album_icon
            width: root.cellWidth
            height: root.cellHeight
            opacity: 0.3
            color: "black"

            z: 2 //BUG DO QT: Força o hightlight permacer no top
        }
    }

    model: listaDeAlbums

    ListAlbums {
        id: listaDeAlbums

        Component.onCompleted: {
            var albumName = ""
            var countSongs = 0
            var indexByAlbum = 0

            for(var i = 0; i < sortedByAlbum.count; i++) {
                if(countSongs == 0) {
                    albumName = sortedByAlbum.get(i).album
                    countSongs = 1;
                    byAlbum.append({album: albumName , numTitle: countSongs});
                }
                else if(albumName == sortedByAlbum.get(i).album) {
                    countSongs++;
                    byAlbum.setProperty(indexByAlbum, "numTitle", countSongs);
                }
                else {
                    albumName = sortedByAlbum.get(i).album
                    countSongs = 1;
                    byAlbum.append({album: albumName , numTitle: countSongs});
                    indexByAlbum++;
                }
            }

            model = byAlbum;
        }
    }

    ListTitles {
        id: listaDeTitulos
    }

    function selectTitleFromAlbum(selectedAlbum) {
        for(var i = 0; i < sortedByAlbum.count; i++) {
            if(selectedAlbum === sortedByAlbum.get(i).album) {
                songsFromAlbum.append(sortedByAlbum.get(i));
            }
        }

        listaDeTitulos.model = songsFromAlbum;
        root.model = listaDeTitulos;
    }
}

