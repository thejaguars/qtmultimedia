import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


GridLayout {
    id: search
    rows: 2
    columns: 2
    rowSpacing: -35
    columnSpacing: 0
    anchors.fill: parent

    KeyNavigation.right: key_clear

    TextField {
        id: search_text
        Layout.row: 1
        Layout.column: 1
        Layout.alignment: Qt.AlignTop
        Layout.preferredHeight: search.height * 0.125
        Layout.preferredWidth: search.width * 0.34
        placeholderText: qsTr("Enter name")

    }


    Item {
        id: search_keyboard
        Layout.row: 2
        Layout.column: 1
        Layout.alignment: Qt.AlignTop
        Layout.preferredHeight: search.height*0.45
        Layout.preferredWidth: parent.width * 0.35

        GridLayout {
            id: keyboard
            rows: 7
            columns: 6
            rowSpacing: parent.width * 0.01
            columnSpacing: parent.width * 0.01
            anchors.fill: parent
            anchors.rightMargin: parent.width * 0.04

            Button {
                id: key_clear
                Layout.columnSpan: 2
                Layout.fillHeight: true
                Layout.fillWidth: true
                Text {text: "limpa"; anchors.centerIn: parent}
                style: ButtonStyle {
                   background: Rectangle {
                       color: {
                           if (key_clear.hovered) {"#efefef"}
                           else if (key_clear.activeFocus) {"#ddd"}
                           else {"transparent"}
                       }
                   }
                }

                KeyNavigation.left: search_btn
                KeyNavigation.right: key_space
            }

            Button {
                id: key_space
                Layout.columnSpan: 2
                Layout.fillHeight: true
                Layout.fillWidth: true
                Text {text: "espaço"; anchors.centerIn: parent}
                style: ButtonStyle {
                   background: Rectangle {
                       color: {
                           if (key_space.hovered) {"#efefef"}
                           else if (key_space.activeFocus) {"#ddd"}
                           else {"transparent"}
                       }
                   }
                }
                KeyNavigation.left: key_clear
                KeyNavigation.right: key_backspace

            }

            Button {
                id: key_backspace
                Layout.columnSpan: 2
                Layout.fillHeight: true
                Layout.fillWidth: true
                Text {text: "<<"; anchors.centerIn: parent}
                style: ButtonStyle {
                   background: Rectangle {
                       color: {
                           if (key_backspace.hovered) {"#efefef"}
                           else if (key_backspace.activeFocus) {"#ddd"}
                           else {"transparent"}
                       }
                   }
                }
                KeyNavigation.left: key_space
                KeyNavigation.right: root
            }

            ListModel {
                id: alphabet
                ListElement { name: "a" }
                ListElement { name: "b" }
                ListElement { name: "c" }
                ListElement { name: "d" }
                ListElement { name: "e" }
                ListElement { name: "f" }
                ListElement { name: "g" }
                ListElement { name: "h" }
                ListElement { name: "i" }
                ListElement { name: "j" }
                ListElement { name: "k" }
                ListElement { name: "l" }
                ListElement { name: "m" }
                ListElement { name: "n" }
                ListElement { name: "o" }
                ListElement { name: "p" }
                ListElement { name: "q" }
                ListElement { name: "r" }
                ListElement { name: "s" }
                ListElement { name: "t" }
                ListElement { name: "u" }
                ListElement { name: "v" }
                ListElement { name: "w" }
                ListElement { name: "x" }
                ListElement { name: "y" }
                ListElement { name: "z" }
                ListElement { name: "1" }
                ListElement { name: "2" }
                ListElement { name: "3" }
                ListElement { name: "4" }
                ListElement { name: "5" }
                ListElement { name: "6" }
                ListElement { name: "7" }
                ListElement { name: "8" }
                ListElement { name: "9" }
                ListElement { name: "0" }
            }

            Repeater {
                model: alphabet
                Button {

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    style: ButtonStyle {
                       background: Rectangle {
                           color: {
                               if (hovered) {"#efefef"}
                               else if (activeFocus) {"#ddd"}
                               else {"transparent"}
                           }
                       }
                    }

                    Text {
                        text: name
                        anchors.centerIn: parent
                        font.family: agfa_rotis_bold.name
                        font.pixelSize: parent.height * 0.85
                        font.bold: true
                    }


                }
            }
        }
    }



    Item {
        id: search_result
        Layout.row: 1
        Layout.column: 2
        Layout.rowSpan: 2
        Layout.fillHeight: true
        Layout.fillWidth: true
        clip: true

        ListView{
            id: root
            anchors.fill: parent

            highlight: Component {
                Rectangle {
                    width: parent.width
                    height: content_wrapper.height * 0.125
                    color: "#ddd"
                    border.color: "#888"
                    border.width: 1
                    radius: parent.height * 0.008
                    visible: root.focus ? true : false
                }
            }

            model: musicsModel

            delegate: Item {
                width: parent.width
                height: content_wrapper.height * 0.125

                GridLayout {
                    rows: 3
                    columns: 3
                    rowSpacing: 0
                    columnSpacing: parent.height * 0.15
                    anchors.leftMargin: parent.height * 0.15
                    anchors.rightMargin: parent.height * 0.15
                    anchors.fill: parent

                    Label {
                        Layout.row: 1
                        Layout.column: 1
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        text: title
                        font.family: agfa_rotis_bold.name
                        font.pixelSize: parent.height * 0.4
                        font.bold: true
                        color: "#444"
                    }

                    Item {
                        Layout.row: 1
                        Layout.column: 3
                        Layout.rowSpan: 2
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }

                    Label {
                        Layout.row: 2
                        Layout.column: 1
                        Layout.fillHeight: true
                        text: artist
                        font.family: agfa_rotis.name
                        font.pixelSize: parent.height * 0.3
                        verticalAlignment: Text.AlignTop
                        color: "#444"
                    }

                    Label {
                        Layout.row: 2
                        Layout.column: 2
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        text: "aaaa"
                        font.family: agfa_rotis.name
                        font.pixelSize: parent.height * 0.3
                        verticalAlignment: Text.AlignTop
                        color: "#444"
                    }

                    Rectangle {
                        Layout.row: 3
                        Layout.column: 1
                        Layout.columnSpan: 3
                        Layout.preferredWidth: parent.width * 0.985
                        Layout.preferredHeight: 1
                        color: "#888"
                    }
                }
            }
        }
    }
}
