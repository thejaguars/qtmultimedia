import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


Rectangle {
    id: teste
    anchors.fill: parent
    color: "transparent"
    Text{
        anchors.top: parent.top
        anchors.topMargin: parent.height*0.05
        anchors.left: parent.left
        anchors.leftMargin:  parent.width*0.05
        text: "Sem playlists"
    }

    KeyNavigation.left: playlists_btn
}
