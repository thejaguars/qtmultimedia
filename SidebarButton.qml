import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Button {
    id: root
    anchors.fill: parent

    property string title: ""
    property string explanation: ""
    property string icon_source: ""
    property string source: ""

    style: ButtonStyle {
        background: Rectangle {
            width: root.width
            height: root.width
            radius: root.height * 0.08
            border.color: (root.hovered | root.activeFocus) ? "#aaa" : "transparent"
            border.width: (root.hovered | root.activeFocus) ? 1 : 0
            color: {
                if(root.hovered) {"#efefef"}
                else if (root.activeFocus) {"#ddd"}
                else {"transparent"}
            }

            Image {
                source: root.icon_source
                width: parent.width * 0.6
                height: parent.height * 0.6
                anchors.centerIn: parent
            }
        }
    }
}
