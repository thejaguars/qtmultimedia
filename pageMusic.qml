import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2

GridLayout {
    id: main_layout
    rows: 3
    columns: 3
    anchors.fill: parent
    columnSpacing: height * 0.02
    rowSpacing: height * 0.02

    property bool playing: false
    property bool shuffle: false
    property bool repeat: false

    signal handlerLoader(string page)

    // ------------
    // HEADER WRAPPER
    // ------------
    Item {
        id: header_wrapper
        Layout.fillWidth: true
        Layout.minimumHeight: parent.height * 0.01
        Layout.preferredHeight: parent.height * 0.1
        Layout.columnSpan: 3
        Layout.alignment: Qt.AlignTop

        // ------------
        // HEADER
        // ------------

        RowLayout {
            id: header
            anchors.fill: parent
            spacing: parent.height * 0.2

            Label {
                id: section
                Layout.fillHeight: true
                text: "Músicas"
                font.pixelSize: parent.height
                font.family:agfa_rotis_bold.name
                font.bold: true
                color: "#444"
            }

            Label {
                id: subsection
                Layout.fillHeight: true
                text: "/ Todas"
                verticalAlignment: Text.AlignBottom
                font.pixelSize: parent.height * 0.5
                font.family:agfa_rotis_bold.name
                color: "#444"
            }

            Image {
                id: logo
                Layout.alignment: Qt.AlignCenter
                Layout.fillHeight: true
                Layout.fillWidth: true
                source: "./icons/logo.svg"
                height: parent.height
                fillMode: Image.PreserveAspectFit
                horizontalAlignment: Image.AlignRight
            }
        }
    }


    // ------------
    // SIDEBAR WRAPPER
    // ------------
    Item {
        id: sidebar_wrapper
        Layout.fillHeight: true
        Layout.minimumWidth: parent.width * 0.07
        Layout.preferredWidth: parent.width * 0.07
        Layout.maximumWidth: sidebar_wrapper.height / 8

        // ------------
        // SIDEBAR
        // ------------
        ColumnLayout {
            id: sidebar
            anchors.fill: parent
            spacing: 0

            function btn_action(btn) {
                subsection.text = btn.title
                footer_txt.text = btn.explanation
                footer_image.source = btn.icon_source
                content.source = btn.source
            }

            Item {
                id: search_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: search_btn
                    title: "/ Pesquisa"
                    explanation: "Busca por nome de música."
                    icon_source: "./icons/search_icon.svg"
                    source: "ContentSearch.qml"

                    KeyNavigation.up: home_btn
                    KeyNavigation.down: allsongs_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(search_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(search_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: allsongs_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: allsongs_btn
                    title: "/ Todas"
                    explanation: "Exibe todas as músicas."
                    icon_source: "./icons/allsongs_icon.svg"
                    source: "ContentSongs.qml"

                    focus: true

                    KeyNavigation.up: search_btn
                    KeyNavigation.down: artists_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(allsongs_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(allsongs_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: artists_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: artists_btn
                    title: "/ Artistas"
                    explanation: "Exibe músicas por artista."
                    icon_source: "./icons/artists_icon.svg"
                    source: "ContentArtists.qml"

                    KeyNavigation.up: allsongs_btn
                    KeyNavigation.down: albuns_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(artists_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(artists_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: albuns_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: albuns_btn
                    title: "/ Álbuns"
                    explanation: "Exibe músicas por álbum."
                    icon_source: "./icons/albuns_icon.svg"
                    source: "ContentAlbuns.qml"

                    KeyNavigation.up: artists_btn
                    KeyNavigation.down: playlists_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(albuns_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(albuns_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: playlists_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: playlists_btn
                    title: "/ Playlists"
                    explanation: "Exibe playlists."
                    icon_source: "./icons/playlists_icon.svg"
                    source: "ContentPlaylists.qml"

                    KeyNavigation.up: albuns_btn
                    KeyNavigation.down: lyric_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(playlists_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(playlists_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: lyric_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: lyric_btn
                    title: "/ Letra"
                    explanation: "Exibe letra de música atual."
                    icon_source: "./icons/lyric_icon.svg"
                    source: "ContentLyric.qml"

                    KeyNavigation.up: playlists_btn
                    KeyNavigation.down: configmus_btn
                    KeyNavigation.right: content.item

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(lyric_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(lyric_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: dummy_box
                Layout.fillHeight: true
            }

            Item {
                id: configmus_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: configmus_btn
                    title: "/ Configuração"
                    explanation: "Configurações de Música."
                    icon_source: "./icons/config_icon.svg"

                    KeyNavigation.up: lyric_btn
                    KeyNavigation.down: home_btn

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(configmus_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(configmus_btn);
                        focus = true
                    }
                }
            }

            Item {
                id: home_btn_wrapper
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop

                SidebarButton {
                    id: home_btn
                    title: "/ Retornar"
                    explanation: "Retorna a tela inicial."
                    icon_source: "./icons/home_icon.svg"

                    KeyNavigation.up: configmus_btn
                    KeyNavigation.down: search_btn

                    onFocusChanged: {
                        if(focus) {
                            sidebar.btn_action(home_btn);
                        }
                    }

                    onClicked: {
                        sidebar.btn_action(home_btn);
                        handlerLoader("pageMainMenu.qml")
                        focus = true
                    }

                    Keys.onEnterPressed: handlerLoader("pageMainMenu.qml")
                    Keys.onReturnPressed: handlerLoader("pageMainMenu.qml")
                }
            }
        }
    }


    // ------------
    // CONTENT WRAPPER
    // ------------
    Item {
        id: content_wrapper
        Layout.fillWidth: true
        Layout.fillHeight: true
        clip: true

        Loader{
            id: content
            anchors.fill: parent;
            source: "ContentSongs.qml"

            //property Item id_return: allsongs_btn
            //setSource(content.item, KeyNavigation.left: allsongs_btn)

            onFocusChanged: { focus ? item.focus = true : item.focus = false; }
        }
    }

    // ------------
    // PLAYER WRAPPER
    // ------------
    Item {
        id: player_wrapper
        Layout.fillHeight: true
        Layout.minimumWidth: parent.height * 0.4
        Layout.preferredWidth: parent.height * 0.4
        Layout.maximumWidth: parent.width * 0.28

        // ------------
        // PLAYER
        // ------------
        ColumnLayout {
            id: tags_wrapper
            anchors.fill: parent
            spacing: 0

            Image {
                id: image_album
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: parent.width
                Layout.alignment: Qt.AlignTop
                source: "./pix/21.jpg"
                anchors.bottomMargin: parent.width * 0.06
            }

            Item {
                id: title_song_wrapper
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true

                PlayerLabel {
                    id: title_song
                    text: "Título"
                    font.pixelSize: parent.width * 0.1
                    font.family: agfa_rotis_bold.name
                    font.bold: true
                }
            }

            Item {
                id: artist_song_wrapper
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true

                PlayerLabel {
                    id: artist_song
                    text: "Artista"
                    font.pixelSize: parent.width * 0.08
                    font.family:agfa_rotis.name
                }
            }

            Item  {
                id: album_song_wrapper
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true

                PlayerLabel {
                    id: album_song
                    text: "Álbum "
                    font.pixelSize: parent.width * 0.08
                    font.family:agfa_rotis.name
                }
            }

            Item {
                id: genre_song_wrapper
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true

                PlayerLabel {
                    id: genre_song
                    text: "Gênero"
                    font.pixelSize: parent.width * 0.08
                    font.family:agfa_rotis.name
                }
            }

            Item {
                id: player_controls_wrapper
                Layout.fillWidth: true
                Layout.minimumHeight: parent.width * 0.25

                RowLayout {
                    id: player_controls
                    anchors.fill: parent
                    spacing: parent.width * 0.08
                    anchors.topMargin: parent.width * 0.04
                    anchors.bottomMargin: parent.width * 0.04

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Button {
                            id: repeat_btn
                            anchors.fill: parent
                            style: ButtonStyle {
                                background: Image {
                                    id: repeat_icon
                                    source: {
                                        if(repeat_btn.pressed) {
                                             "./icons/repeat_disable_icon.svg";
                                        }
                                        else if (repeat_btn.hovered){
                                             "./icons/repeat_hover_icon.svg";
                                        }
                                        else {
                                             if(repeat) {
                                                  "./icons/repeat_icon.svg";
                                             }
                                             else {
                                                 "./icons/repeat_disable_icon.svg";
                                             }
                                        }
                                    }
                                    width: parent.width
                                    fillMode: Image.PreserveAspectFit
                                    anchors.centerIn: parent
                                }
                            }

                            onClicked: {repeat ? repeat = false : repeat = true}
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Button {
                            id: prev_btn
                            anchors.fill: parent
                            style: ButtonStyle {
                                background: Image {
                                    id: prev_icon
                                    source: {
                                        if(prev_btn.pressed) {
                                             "./icons/prev_pressed_icon.svg";
                                        }
                                        else if (prev_btn.hovered){
                                             "./icons/prev_hover_icon.svg";
                                        }
                                        else {
                                             "./icons/prev_icon.svg";
                                        }
                                    }
                                    width: parent.width
                                    fillMode: Image.PreserveAspectFit
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Item {
                        Layout.preferredWidth: parent.height
                        Layout.maximumWidth: parent.width * 0.2
                        Layout.fillHeight: true

                        Button {
                            id: play_btn
                            anchors.fill: parent
                            style: ButtonStyle {
                                background: Image {
                                    id: play_icon
                                    source: {
                                        if(play_btn.pressed) {
                                             playing ? "./icons/pause_pressed_icon.svg" : "./icons/play_pressed_icon.svg"
                                        }
                                        else if (play_btn.hovered){
                                             playing ? "./icons/pause_hover_icon.svg" : "./icons/play_hover_icon.svg"
                                        }
                                        else {
                                             playing ? "./icons/pause_icon.svg" : "./icons/play_icon.svg"
                                        }
                                    }
                                    width: parent.width
                                    fillMode: Image.PreserveAspectFit
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Button {
                            id: next_btn
                            anchors.fill: parent
                            style: ButtonStyle {
                                background: Image {
                                    id: next_icon
                                    source: {
                                        if(next_btn.pressed) {
                                             "./icons/next_pressed_icon.svg";
                                        }
                                        else if (next_btn.hovered){
                                             "./icons/next_hover_icon.svg";
                                        }
                                        else {
                                             "./icons/next_icon.svg";
                                        }
                                    }
                                    width: parent.width
                                    fillMode: Image.PreserveAspectFit
                                    anchors.centerIn: parent
                                }
                            }
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Button {
                            id: shuffle_btn
                            anchors.fill: parent
                            style: ButtonStyle {
                                background: Image {
                                    id: search_icon
                                    source: {
                                        if(shuffle_btn.pressed) {
                                             "./icons/shuffle_disable_icon.svg";
                                        }
                                        else if (shuffle_btn.hovered){
                                             "./icons/shuffle_hover_icon.svg";
                                        }
                                        else {
                                             if(shuffle) {
                                                 "./icons/shuffle_icon.svg";
                                             }
                                             else {
                                                  "./icons/shuffle_disable_icon.svg";
                                             }
                                        }
                                    }
                                    width: parent.width
                                    fillMode: Image.PreserveAspectFit
                                    anchors.centerIn: parent
                                }
                            }

                            onClicked: {shuffle ? shuffle = false : shuffle = true}
                        }
                    }
                }
            }

            Item {
                id: progress_bar_wrapper
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignBottom

                GridLayout {
                    id: progress_bar_player
                    rows: 2
                    columns: 2
                    rowSpacing: 0
                    columnSpacing: parent.width * 0.01
                    width: parent.width
                    height: parent.width * 0.11
                    anchors.bottom: parent.bottom


                    ProgressBar {
                        id: progress_bar
                        Layout.columnSpan: 2
                        Layout.preferredHeight: parent.width * 0.04
                        Layout.fillWidth: true
                        value: 0

                        style: ProgressBarStyle {
                            background: Rectangle {
                                color: "#ddd"
                                border.color: "#888"
                                border.width: 1
                            }
                            progress: Rectangle {
                                color: "#666"
                                border.color: "#666"
                            }
                        }
                    }

                    Label {
                        id: time_counter
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        text: "-:--"
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignBottom
                        font.pixelSize: parent.width * 0.06
                        font.family:agfa_rotis.name
                        color: "#444"
                        property int val: 0
                    }

                    Label {
                        id: time_end
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        text: "-:--"
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignBottom
                        font.pixelSize: parent.width * 0.06
                        font.family:agfa_rotis.name
                        color: "#444"
                        property int val: 0
                    }
                }
            }
        }
    }

    // ------------
    // FOOTER WRAPPER
    // ------------

    Item {
        id: footer_wrapper
        Layout.fillWidth: true
        Layout.minimumHeight: parent.height * 0.08
        Layout.preferredHeight: parent.height * 0.08
        Layout.maximumHeight: parent.width * 0.06
        Layout.columnSpan: 3
        Layout.alignment: Qt.AlignBottom

        // ------------
        // FOOTER
        // ------------
        Rectangle {
            id: footer
            anchors.fill: parent
            radius: parent.height * 0.008
            border.color : "#aaa"
            border.width : 1
            color: "#eee"

            RowLayout {
                anchors.fill: parent
                anchors.margins: parent.height * 0.3
                spacing: parent.height * 0.3

                Image {
                    id: footer_image
                    source: "./icons/allsongs_icon.svg"
                    Layout.preferredHeight: parent.height
                    Layout.preferredWidth: parent.height
                }

                Label {
                    id: footer_txt
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    text: "Exibe todas as músicas."
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: parent.height
                    font.family:agfa_rotis.name
                    color: "#444"
                }
            }
        }
    }
}
