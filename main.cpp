#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QList>
#include <QDebug>
#include <QQuickView>
#include <QQmlContext>
#include <QDir>
#include <QStringRef>
#include <QFile>
#include "dataobject.h"
#include <taglib.h>
#include <QtAlgorithms>
#include <QStorageInfo>
//QMediaPlayer DataObject::m_player;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QList<QObject*> dataList;

//    QStringList titlesNames;
//    QStringList albumsNames;
//    QStringList artistasNames;
    QList<QStorageInfo>storages = QStorageInfo::mountedVolumes();
    for(int i=0;i<storages.length();i++){

        if(storages.at(i).isValid()){
            qInfo() << storages.at(i).rootPath();
             QDir *mDir = new QDir(storages.at(i).rootPath());
             if(!mDir->exists())
                 qInfo()<<"Media not attached";
             else{
                 qInfo()<<"Media attached";
                 QStringList filters;
                 filters <<"*.mp3";
                 mDir->setNameFilters(filters);
                 QStringList filesName = mDir->entryList();
                 //qInfo()<< "Quantidade de arquivos .mp3 na pasta: " << filesName.length();
                 for(int i=0;i<=filesName.length()-1;i++){
                     dataList.append(new DataObject(new QObject(),mDir,filesName.at(i)));

                 }


             }

        }
    }
 //   QDir *mDir = new QDir("C:/Users/Gil/Desktop/musicas/");
//    QDir *mDir = new QDir("/Volumes/Paolla_1Tb/Desenvolvimento/Qt/qtmultimedia/music");
//    QDir *mDir = new QDir("D:/Minhas Músicas/DIVERSOS/Internacional");

    //QDir *mDir = new QDir("/home/embarcados/Música");
//    QDir *mDir = new QDir("/home/pi/Music");

//    if(!mDir->exists())
//        qInfo()<<"Media not attached";
//    else{
//        qInfo()<<"Media attached";
//        QStringList filters;
//        filters <<"*.mp3";
//        mDir->setNameFilters(filters);
//        QStringList filesName = mDir->entryList();
//        //qInfo()<< "Quantidade de arquivos .mp3 na pasta: " << filesName.length();
//        for(int i=0;i<=filesName.length()-1;i++){
//            dataList.append(new DataObject(new QObject(),mDir,filesName.at(i)));

//        }


//    }

    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();

//    QVariant vari = QVariant::fromValue(dataList);
//    QVariant albumsModel = QVariant::fromValue(albumsNames);
//    QVariant artistsModel = QVariant::fromValue(artistasNames);
//    ctxt->setContextProperty("musicsModel", vari);
//    ctxt->setContextProperty("albumsModel", albumsModel);
//    ctxt->setContextProperty("artistsModel", artistsModel);

    ctxt->setContextProperty("musicsModel", QVariant::fromValue(dataList));
    engine.load(QUrl("qrc:/main.qml"));
    QObject::connect(&engine, SIGNAL(quit()), &app, SLOT(quit())); // to make Qt.quit() to work.
    //engine.load(QUrl("qrc:/view.qml"));

    return app.exec();
}

